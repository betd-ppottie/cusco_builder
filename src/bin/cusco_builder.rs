extern crate cusco_builder_lib;
use cusco_builder_lib::template_loader::template_parsing::TemplateParsing;
use std::error::Error;







fn main() -> Result<(), Box<dyn Error>> {

   let common_type = std::env::args().nth(1).expect("no type given");

   let mut parser : TemplateParsing = TemplateParsing::new();

    parser.parse(&common_type);

   

    Ok(())
}