use serde::Deserialize;
use std::fs;
use std::path::PathBuf;

#[derive(Debug, Deserialize)]
pub struct TemplateFile {
    pub template_path: String,
    pub namefile: String,
    pub content: String,
}

impl TemplateFile {
    pub fn new() -> Self {
        Self {
            template_path: String::new(),
            namefile: String::new(),
            content: String::new(),
        }
    }

    pub fn get_filename(&self) -> String {
        self.namefile.clone()
    }
    pub fn set_template_path(&mut self, path: &PathBuf) {
        self.template_path = path.clone().into_os_string().into_string().unwrap();
        self.namefile = path.file_name().unwrap().to_os_string().into_string().unwrap();
    }

    pub fn load_content(&mut self) {
        self.content = fs::read_to_string(&self.template_path.clone()).expect(&format!("{} not found ",self.template_path));
    }


    pub fn set_content(&mut self, content: String) {
        self.content = content.clone();
    }
}

#[cfg(test)]
mod tests {

    use super::*;
    static COMMON_TEST_FILE:  &'static str =  "_test/common/test.common.test";
    static CONTENT_COMMON_TEST_FILE:  &'static str =  "# common content";

     #[test]
     fn set_template_path(){
         let mut commonfile: TemplateFile= TemplateFile::new();
         commonfile.set_template_path( & PathBuf::from(COMMON_TEST_FILE));   
         assert_eq!( COMMON_TEST_FILE,commonfile.template_path);
         assert_eq!( "test.common.test".to_string(),commonfile.namefile);
     }
    #[test]
     fn get_filename(){
        let mut commonfile: TemplateFile= TemplateFile::new();
        commonfile.set_template_path( & PathBuf::from(COMMON_TEST_FILE));   
        assert_eq!( "test.common.test".to_string(),commonfile.get_filename());
    }

    #[test]
    fn load_content_file(){
       let mut commonfile: TemplateFile= TemplateFile::new();
       commonfile.set_template_path( & PathBuf::from(COMMON_TEST_FILE));   
       commonfile.load_content();
       assert_eq!( CONTENT_COMMON_TEST_FILE,commonfile.content);
   }
}