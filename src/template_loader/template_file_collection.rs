use std::{collections::HashMap, borrow::Borrow};
use glob::{glob_with, MatchOptions};
use serde::Deserialize;
use crate::template_loader::template_file::TemplateFile;

static COMMON_PATTERN_NAME:  &'static str = "common";
static CUSTOM_PATTERN_NAME: &'static str = "custom";

#[derive(Debug,Deserialize)]
pub  struct TemplateFileCollection {
    collection: HashMap<String,TemplateFile>
}

impl TemplateFileCollection {
    pub fn new() -> Self{
        Self {
            collection: HashMap::<String,TemplateFile>::new(),
        }
    }

    pub fn get_map(&self) -> &HashMap<std::string::String, TemplateFile>{
        self.collection.borrow().clone()
    }
    
    pub fn find_template_files(&mut self, template_type: &str, file_type: &str)-> &HashMap<std::string::String, TemplateFile> {
        let options = MatchOptions {
            case_sensitive: false,
            ..Default::default()
        };
        for entry in glob_with(&format!("{}/{}/*.{}.*", template_type, file_type,template_type ), options).unwrap() {
            if let Ok(path) = entry {
                let mut template_file= TemplateFile::new();
            
                template_file.set_template_path(&path);
                template_file.load_content();
                self.collection.entry(template_file.get_filename()).or_insert(template_file);
            }
        }
        
        self.collection.borrow().clone()
    }

    pub(crate) fn find_custom_template_files(&mut self, template_type: &str) -> &HashMap<std::string::String, TemplateFile> {
        self.find_template_files(CUSTOM_PATTERN_NAME, template_type)
    }

    pub(crate) fn find_common_template_files(&mut self, template_type: &str) -> &HashMap<std::string::String, TemplateFile> {
        self.find_template_files(COMMON_PATTERN_NAME, template_type)
    }
}