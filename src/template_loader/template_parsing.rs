use std::{borrow::Borrow, path::PathBuf, fs};
use crate::template_loader::template_file_collection::TemplateFileCollection;
use serde::Deserialize;

use super::template_file::TemplateFile;

#[derive(Debug, Deserialize)]
pub struct TemplateParsing {
    collection: TemplateFileCollection,
    destination: TemplateFile,
}

impl  TemplateParsing {
    pub fn new() -> Self{
        Self {
            collection: TemplateFileCollection::new(),
            destination: TemplateFile::new(),
        }
    }

    fn load_templates_collection(&mut self,template_type: &str){
       self.collection.find_common_template_files(template_type);
       self.collection.find_custom_template_files(template_type);
    }

    fn load_destination_file(&mut self, filename: &str){
        self.destination.set_template_path( &PathBuf::from(filename).with_extension("template"));
        self.destination.load_content();
    }

    pub fn parse(&mut self,template_type: &str)-> &TemplateFileCollection {
       self.load_templates_collection(template_type);
       self.load_destination_file(template_type);
       self.replace_tags_by_templates_collection();
       self.replace_destination(template_type);
       self.collection.borrow().clone()
    }

    fn replace_tags_by_templates_collection(&mut self) {
        for (key, template_file) in self.collection.get_map().iter(){
            let tag_name: String = key.as_str().to_uppercase();
            self.destination.set_content( self.destination.content.replace(&format!("### {} ###",tag_name.replace(".","_") ),  &template_file.content.to_string()));
            println!("tag {} traité", tag_name.replace(".","_"));
  
        }

    }

    fn replace_destination(&self, filename: &str)  {
        fs::write(filename.to_string(),self.destination.content.to_string()).unwrap();
    }   

}